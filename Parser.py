import os
from dotenv import load_dotenv
import requests
import bs4
from datetime import datetime, timedelta
import csv
import time
from selenium import webdriver


load_dotenv()


def load_page(index=1):
    headers = get_headers()
    with requests.Session() as s:
        response = s.get('https://www.investing.com/equities/yandex-news/' + str(index), headers=headers)
    return response.text


def get_headers():
    headers = {}
    headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,' \
                        '*/*;q=0.8,application/signed-exchange;v=b3;q=0.9 '
    headers['Accept-Encoding'] = 'gzip, deflate, br'
    headers['Accept-Language'] = 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
    headers['User-Agent'] = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, ' \
                            'like Gecko) Chrome/85.0.4183.121 Mobile Safari/537.36 '

    return headers


def save_page_to_file(text, filename="page.txt"):
    text_file = open(filename, "w", encoding='utf-8')
    text_file.write(text)
    text_file.close()


def read_page_from_file(filename="page.txt"):
    text_file = open(filename, "r", encoding='utf-8')
    text = text_file.read()
    text_file.close()

    return text


def parse_page(text):
    soup = bs4.BeautifulSoup(text, 'lxml')
    # find all articles
    articles_result_set = soup.find_all('div', class_='mediumTitle1')[1].find_all('article', class_='js-article-item')
    for parsed_article in articles_result_set:
        article = {}
        article['headline'] = parsed_article.find('a', class_='title').string  # headline of news
        article['link'] = parsed_article.find('a', class_='title')['href']  # link to full text paper
        article_details = parsed_article.find('span', class_='articleDetails')
        article['source'] = article_details.find('span').string
        try:
            # date of news
            article['date'] = datetime.strptime(article_details.find('span', class_='date')
                                                .string.replace("\xa0-\xa0", ''), '%b %d, %Y')
        except ValueError:
            # 'x days ago' to datetime
            parsed_s = [article_details.find('span', class_='date').string.replace("\xa0-\xa0", '').split()[:2]]
            time_dict = dict((fmt, float(amount)) for amount, fmt in parsed_s)
            dt = timedelta(**time_dict)
            article['date'] = datetime.now() - dt
        except Exception:
            article['date'] = datetime(1970, 1, 1)
            print('Somthing go wrong with date of news. Filled by default value')
        yield article
    print(len(articles_result_set))


def last_page(text):
    soup = bs4.BeautifulSoup(text, 'lxml')
    # find next button
    div = soup.find('div', class_='sideDiv inlineblock text_align_lang_base_2')
    if div.find('a'):
        return False
    return True


def save_data_to_csv(data, filename='data.csv'):
    keys = data[0].keys()
    with open(filename, 'w', encoding="utf-8", newline='') as output_file:
        dict_writer = csv.DictWriter(output_file, keys)
        dict_writer.writeheader()
        dict_writer.writerows(data)


articles = []

options = webdriver.ChromeOptions()
options.add_argument('headless')
driver = webdriver.Chrome(executable_path=os.environ.get('PATH_TO_CHROME_DRIVER'),
                          options=options)

for i in range(1, 100):
    driver.get("https://www.investing.com/equities/yandex-news/" + str(i))
    time.sleep(1)
    for article in parse_page(driver.page_source):
        if len(articles) == 0 or articles[-1]['headline'] != article['headline']:
            articles.append(article)
    else:
        if not last_page(driver.page_source):
            continue
    break

driver.close()

save_data_to_csv(articles, '../data/investing-data.csv')
